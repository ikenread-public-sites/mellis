const API_URL = "https://localhost:5001/inquiries";

function toggleMute() {
  const video = document.getElementById("fws-commercial");
  const icon = document.getElementById("volume-icon");

  if (video.muted) {
    video.muted = false;
    icon.classList.remove("fa-volume-up");
    icon.classList.add("fa-volume-off");
  } else {
    video.muted = true;
    icon.classList.add("fa-volume-up");
    icon.classList.remove("fa-volume-off");
  }
}

function fullscreen() {
  var video = document.getElementById("fws-commercial");

  if (video.requestFullscreen) {
    video.requestFullscreen();
  } else if (video.webkitRequestFullscreen) {
    /* Safari */
    video.webkitRequestFullscreen();
  } else if (video.msRequestFullscreen) {
    /* IE11 */
    video.msRequestFullscreen();
  }
}

function rotate(classname, reverse = false) {
  const slides = document.getElementsByClassName(classname);
  const activeSlides = document.getElementsByClassName(classname + " active");

  for (let i = 0; i < activeSlides.length; i++) {
    const id = activeSlides[i].id;

    // Set next ID number
    let num = Number(id.slice(-1));
    num = reverse ? --num : ++num;
    num = num < 1 ? slides.length : num;

    let nextId = id.slice(0, -1) + num;
    let nextSlide = document.getElementById(nextId);

    if (!nextSlide) {
      nextId = id.slice(0, -1) + 1;
      nextSlide = document.getElementById(nextId);
    }

    activeSlides[i].classList.remove("active");
    nextSlide.classList.add("active");
  }
}

function rotateSlides(reverse = false) {
  clearInterval(slideInterval);
  rotate("slide", reverse);
  slideInterval = window.setInterval(rotateSlides, 3000);
}

function onRecaptchaFail() {
  recaptchaToken = "";
  const error = document.getElementById("error");
  error.classList.remove("hide");
  error.innerText = "Recaptcha Failed. Please try again.";
}

function onRecaptchaSuccess(token) {
  const submit = document.getElementById("submit-button");
  submit.disabled = false;
  recaptchaToken = token;
}

function onAttachmentsChange() {
  let attachments = document.getElementById("attachments");
  let text = document.getElementById("attachment-text");

  text.innerText = attachments.files.length + " FILE(S) UPLOADED";
}

function sendRequest() {
  // Hides error message
  const error = document.getElementById("error");
  if (!error.classList.contains("hide")) error.classList.add("hide");

  this.toggleSpinner(true);

  let name = document.getElementById("name");
  let location = document.getElementById("location");
  let phone = document.getElementById("phone");
  let email = document.getElementById("email");
  let message = document.getElementById("message");
  let attachments = document.getElementById("attachments");

  const data = new FormData();
  data.append("name", name.value);
  data.append("location", location.value);
  data.append("phone", phone.value);
  data.append("email", email.value);
  data.append("message", message.value);
  data.append("token", recaptchaToken);

  for (let i = 0; i < attachments.files.length || i == 5; i++) {
    const file = attachments.files[i];

    // Don't upload larger than 15MB
    if (file.size > 15728640) {
      attachments.value = "";
      const error = document.getElementById("error");
      error.classList.remove("hide");
      error.innerText =
        "Images can not be larger than 15MB, please add your images again.";
      return;
    }

    data.append("files", file);
  }

  fetch(API_URL, {
    method: "POST",
    body: data,
  })
  .then(() => {
    this.toggleSpinner(false);
    grecaptcha.reset();

    // Clear Form
    name.value =
      location.value =
      phone.value =
      email.value =
      message.value =
      attachments.value =
        "";

    let text = document.getElementById("attachment-text");
    text.innerText = "UPLOAD ATTACHMENTS (5 MAX)";

    // Success Message
    const success = document.getElementById("success");
    success.classList.remove("hide");
    success.innerText =
      "Your request had been submitted. A member of our team will reach out to you shortly.";
    setTimeout(() => {
      success.classList.add("hide");
    }, 5000);
  })
  .catch(() => {
    this.toggleSpinner(false);
    grecaptcha.reset();

    // Error Message
    const success = document.getElementById("success");
    success.classList.remove("hide");
    success.innerText =
      "There was a problem with your request. Please email us directly at sales@fwslift.com";
    setTimeout(() => {
      success.classList.add("hide");
    }, 10000);
  });
}

function toggleSpinner(show) {
  let text = document.getElementById("submit-text");
  let spinner = document.getElementById("submit-spinner");

  if (show) {
    text.style.visibility = "hidden";
    spinner.style.visibility = "visible";
  } else {
    text.style.visibility = "visible";
    spinner.style.visibility = "hidden";
  }
}

function openDemoModal(videoId) {
  let modal = document.getElementById("demo-modal");
  let video = document.getElementById(videoId);

  modal.style.display = "block";
  video.style.display = "block";

  video.currentTime = 0;
  video.play();
}

function closeDemoModal() {
  let modal = document.getElementById("demo-modal");
  let videos = document.getElementsByClassName("demo-video");

  modal.style.display = "none";

  for (let i = 0; i < videos.length; i++) {
    videos[i].style.display = "none";
    videos[i].pause()
  }
}

// Initiate Recaptcha
let recaptchaToken = "";

// Initiate Slides
let slideInterval = window.setInterval(rotateSlides, 3000);
window.setInterval(() => {
  rotate("testimonial");
}, 5000);

// Initiate Demo Modal Close
window.onclick = function(event) {
  let modal = document.getElementById("demo-modal");
  if (event.target === modal) {
    closeDemoModal();
  }
}
